# signals-for-single-railroad-track

# SCOPE:

In this project, we will develop a signal system for a railroad track to avoid accidents caused by multiple trains coming at the same time on the same track.

# DESCRIPTION: 

We will develop a signal system to protect the single-railroad-track section. Assume, that there are two tracks on each end - one for trains entering the single track, and one for trains leaving the single track so there migth be deadlocks caused by trains blocking each other.
Our System will detect the incoming trains through sensors and provides singals to the to the LEDs which helps the other incoming trains to see weather the track is empty or not.

# COMPONENTS:

1. Arduino UNO
2. Breadboard
3. IR Infrared Obstacle Avoidance Sensor 
4. Breadboard Jumper Wires
5. Breadboard Jumper Wires
6. LED lights (red, green, yellow)
7. USB cable

# SCHEMATIC DIAGRAM:

![Arduino-train-crossing-0007-wiring-diagram](/uploads/0f1a9a4bbb1c0a5fb90ea997d38954e6/Arduino-train-crossing-0007-wiring-diagram.jpg)